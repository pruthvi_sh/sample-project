We recommend that you follow the template set out below, when creating your Tutorial. Your tutorial should be sub-divided into sections. ALL sections should be titled with `<H1>` tags or the markdown equivalent `#`
<h1>INSTALLATION OF MINISHIFT LOCALLY</h1>
This is the title of your tutorial, it should be written with `<H1>` tags or the markdown equivalent `#`

# Objective
the objective of this tutorial is to learn how one can install minishift on to the local cluster

# Prerequisite
your system must have 4gb ram and hyper-v installed

# Section 1
Install Minishift on Windows 10 with Hyper-V
1) Setting Up the Hyper-V Driver
Install Hyper-V in Windows 10.
Add the user to the local Hyper-V Administrators group.
PS command : 
([adsi]"WinNT://./Administrateurs Hyper-V,group").Add("WinNT://$env:UserDomain/$env:Username,user") 

– Add an External Virtual Switch. 

Identify first the net adapter to use. 
PS command : 
Get-NetAdapter 

Then store which one you want to use for Minishift network access.
PS command : 
$net = Get-NetAdapter -Name 'Wi-Fi' A

At last create a virtual switch for Hyper-V : 
New-VMSwitch -Name "External VM Switch" -AllowManagementOS $True -NetAdapterName $net.Name
Minishift has to aware of the external virtual switch to use.
You can set its name from the Minishift command arg or as a environment variable.  

In theory, that should be enough but it will not the case on Windows 10: 
minishift start --hyperv-virtual-switch "External VM Switch" 

This command execution downloads the Minishift ISO but (in my case at least) fails when it tries to retrieve the downloaded ISO on Windows 10 because of the path specified in the running script that relies on the Linux based filesystem layout. 
After the failure, a simple workaround is to move the downloaded ISO where you like and to refer it as you execute the start command of the Minishift executable. 
It would give :
minishift start --hyperv-virtual-switch "External VM Switch" --iso-url file://D:/minishift/minishift-centos7.iso 
Where you should refer in the iso-url  parameter the file URI (in your local drive) of the Minishift ISO.
3) Stop Minishift
That releases memory/cpu resources allocated to Minishift by stopping it : 
minishift stop 
It allows you to restart it fast later. 
4) Delete Minishift

A weird issue with Minishift, the need to start from scratch or simply you don’t need it any longer : delete files associated to the Minishift instance/cluster : minishift delete --force



